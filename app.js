var express = require("express");
var app = express();
var path = require("path");
var port = 8000;

app.use("/",function(res,req,next){
    console.log("this is second output");
    next();   
 
});

app.get("/",function(req,res){
    console.log("this is first output");
});

app.listen(port,function(){
    console.log("listening at port 8000..")
}); 