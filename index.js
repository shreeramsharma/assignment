require("dotenv").config();
const express = require("express");
const logger = require("./config/logger");
const app = express();

app.use(express.json());


const port = process.env.PORT || 4000;
app.listen(port, () => {
  logger.log('info',`server up and running on PORT : ${port}`);
});